<?php

class M_Produk extends MY_Model
{
    protected $table = 'barang';
    protected $schema = '';
    public $key = 'idbarang';
    public $value = 'namabarang';

    public function getProdukByKey($key, $idusaha)
    {
        $q = "SELECT b.namabarang,b.harga_jual,b.diskon,b.stok,b.idkategori,b.gambar,b.deskripsi,jb.namajenis FROM barang b JOIN kategori USING(idkategori) JOIN jenisbarang jb USING(idjenis) JOIN merk USING(idmerk) WHERE b.idkategori='$key' AND b.idusaha=$idusaha";
        $get = $this->db->query($q);

        return $get;
    }

    public function searchProdukByKeyword($keyword, $key, $idusaha = null)
    {
        $cond = !empty($idusaha) ? " AND b.idusaha=$idusaha" : "";
        $q = "SELECT b.namabarang,b.harga_jual,b.diskon,b.stok,b.idkategori,b.gambar,b.deskripsi,jb.namajenis FROM barang b JOIN kategori USING(idkategori) JOIN jenisbarang jb USING(idjenis) JOIN merk USING(idmerk) WHERE b.namabarang LIKE '%$keyword%' AND b.idkategori='$key'" . $cond;
        $get = $this->db->query($q);

        return $get;
    }

    public function sortProdukByKey($key, $typesort, $idusaha = null, $keyword = null)
    {
        $cond = !empty($keyword) && !empty($idusaha) ? " AND b.idusaha=$idusaha AND b.namabarang LIKE '%$keyword%' ORDER BY b.harga_jual " : !empty($keyword) ? " AND b.namabarang LIKE '%$keyword%' ORDER BY b.harga_jual " : " AND b.idusaha=$idusaha ORDER BY b.harga_jual ";
        $q = "SELECT b.namabarang,b.harga_jual,b.diskon,b.stok,b.idkategori,b.gambar,b.deskripsi,jb.namajenis FROM barang b JOIN kategori USING(idkategori) JOIN jenisbarang jb USING(idjenis) JOIN merk USING(idmerk) WHERE b.idkategori='$key'" . $cond . $typesort;
        $get = $this->db->query($q);

        return $get;
    }

    public function getProdukByName($nama)
    {
        $val = $this->db->get_where('barang', ['namabarang' => $nama])->row_array();

        return $val;
    }

    public function getProdukByTrans($key = null, $iduser = null)
    {
        $query_utrans = "SELECT SUM(td.jumlah) AS terjual,b.idbarang,b.namabarang,b.harga_jual,b.diskon,b.stok,b.idkategori,b.gambar,b.deskripsi,jb.namajenis,us.namausaha,us.latitude,us.longitude FROM transaksi_detail td JOIN transaksi tr ON td.kodetransaksi=tr.kodetransaksi JOIN barang b ON td.idbarang=b.idbarang JOIN jenisbarang jb ON b.idjenis=jb.idjenis JOIN usaha us ON b.idusaha=us.idusaha WHERE tr.iduser=$iduser AND b.idkategori=$key AND tr.idstatus=4 GROUP BY td.idbarang ORDER BY terjual DESC LIMIT 9";

        $user_trans = $this->db->query($query_utrans);

        if ($user_trans->num_rows() >= 9) {
            return $user_trans;
        } else {
            $query_trans = "SELECT SUM(td.jumlah) AS terjual,b.idbarang,b.namabarang,b.harga_jual,b.diskon,b.stok,b.idkategori,b.gambar,b.deskripsi,jb.namajenis,us.namausaha,us.latitude,us.longitude FROM transaksi_detail td JOIN transaksi tr ON td.kodetransaksi=tr.kodetransaksi JOIN barang b ON td.idbarang=b.idbarang JOIN jenisbarang jb ON b.idjenis=jb.idjenis JOIN usaha us ON b.idusaha=us.idusaha WHERE b.idkategori=$key AND tr.idstatus=4 GROUP BY td.idbarang ORDER BY terjual DESC LIMIT 9";

            $produk_trans = $this->db->query($query_trans);

            if ($produk_trans->num_rows() >= 9) {
                return $produk_trans;
            } else {
                $query_produk = "SELECT b.idbarang,b.namabarang,b.harga_jual,b.diskon,b.stok,b.idkategori,b.gambar,b.deskripsi,jb.namajenis,us.namausaha,us.latitude,us.longitude FROM barang b JOIN jenisbarang jb ON b.idjenis=jb.idjenis JOIN usaha us ON b.idusaha=us.idusaha WHERE b.idkategori=$key LIMIT 9";

                return $this->db->query($query_produk);
            }
        }
    }
}
