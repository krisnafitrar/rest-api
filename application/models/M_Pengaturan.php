<?php


class M_Pengaturan extends CI_Model
{

    public function get()
    {
        $get = $this->db->get('pengaturan')->result_array();

        return $get;
    }

    public function getByKey($key)
    {
        $get = $this->db->get_where('pengaturan', ['idpengaturan' => $key])->row_array();

        return $get;
    }
}
