<?php


class M_Users extends CI_Model
{

    public function getUsers()
    {
        $user = $this->db->get('users')->result_array();

        return $user;
    }

    public function getUserByKey($key)
    {
        $user = $this->db->get_where('users', ['notlp' => $key])->row_array();

        if ($user) {
            return $user;
        } else {
            return 0;
        }
    }

    public function countByKey($key)
    {
        $x = $this->db->get_where('users', ['notlp' => $key])->num_rows();

        return $x;
    }

    public function addNewUser($data)
    {
        $add = $this->db->insert('users', $data);

        if ($add) {
            return 1;
        } else {
            return 0;
        }
    }

    public function updateUser($data, $key)
    {
        $update = $this->db->update('users', $data, ['notlp' => $key]);

        if ($update) {
            return 1;
        } else {
            return 0;
        }
    }
}
