<?php

class M_Ongkir extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function get()
	{
		return $this->db->get('ongkir')->result_array();
	}

	public function getByKey($key)
	{
		return $this->db->get_where('ongkir',['idongkir' => $key])->row_array();
	}


}