<?php

class M_BarangFav extends CI_Model
{

    public function countProdukFav($id, $idbarang)
    {
        $query = "SELECT * FROM barang_favorit WHERE iduser='$id' AND idbarang='$idbarang'";

        return $this->db->query($query)->num_rows();
    }

    public function deleteFav($id, $idbarang)
    {
        $query = "DELETE FROM barang_favorit WHERE iduser='$id' AND idbarang='$idbarang'";
        $ok = $this->db->query($query);

        if ($ok) {
            return true;
        } else {
            return false;
        }
    }

    public function getProdukFav($id)
    {
        $query = "SELECT * FROM barang_favorit bf JOIN barang b USING(idbarang) JOIN jenisbarang jb ON b.idjenis=jb.idjenis JOIN kategori k ON b.idkategori=k.idkategori WHERE bf.iduser=$id";

        return $this->db->query($query)->result_array();
    }
}
