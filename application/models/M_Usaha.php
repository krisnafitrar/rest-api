<?php

class M_Usaha extends MY_Model
{
    protected $table = 'usaha';
    protected $schema = '';
    public $key = 'idusaha';
    public $value = 'namausaha';

    public function __construct()
    {
        parent::__construct();
    }

    public function getMart()
    {   
        $query = "SELECT us.namausaha,us.gambar,us.alamatusaha,us.latitude,us.longitude,js.jenisusaha,pv.provinsi,kt.kota FROM usaha us JOIN jenis_usaha js ON us.idjenisusaha=js.idjenisusaha JOIN provinsi pv ON us.idprovinsi=pv.idprovinsi JOIN kota kt ON us.idkota=kt.idkota";
        return $this->db->query($query);
    }

    public function searchSwalayan($keyword)
    {
        $query = "SELECT us.namausaha,us.gambar,us.alamatusaha,us.latitude,us.longitude,js.jenisusaha,pv.provinsi,kt.kota FROM usaha us JOIN jenis_usaha js ON us.idjenisusaha=js.idjenisusaha JOIN provinsi pv ON us.idprovinsi=pv.idprovinsi JOIN kota kt ON us.idkota=kt.idkota WHERE us.namausaha LIKE '%$keyword%'";
        return $this->db->query($query);
    }

}