<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class ProdukFav extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_BarangFav', 'barangfav');
    }

    public function index_get()
    {
        $id = $this->get('id');
        $kodebarang = $this->get('idbarang');
        $act = $this->get('act');

        switch ($act) {
            case 'get':
                $get = $this->barangfav->getProdukFav($id);
                if ($get) {
                    $this->response($get);
                } else {
                    $this->response([
                        'status' => false
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
                break;
            case 'count':
                $count = $this->barangfav->countProdukFav($id, $kodebarang);

                if ($count < 1) {
                    $this->response([
                        'status' => true
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => false
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
                break;
        }
    }

    public function index_post()
    {
        $id = $this->post('id');
        $idbarang = $this->post('idbarang');

        $get = $this->barangfav->countProdukFav($id, $idbarang);

        if ($get < 1) {
            $ok = $this->db->insert('barang_favorit', ['iduser' => $id, 'idbarang' => $idbarang]);

            if ($ok) {
                $this->response([
                    'status' => true,
                    'act' => 'insert'
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                ], REST_Controller::HTTP_EXPECTATION_FAILED);
            }
        } else {
            $del = $this->barangfav->deleteFav($id, $idbarang);

            if ($del) {
                $this->response([
                    'status' => true,
                    'act' => 'delete'
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                ], REST_Controller::HTTP_EXPECTATION_FAILED);
            }
        }
    }
}
