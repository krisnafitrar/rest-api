<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Users extends REST_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Users');;
    }

    public function index_get()
    {
        $users = $this->M_Users->getUsers();
        $key = $this->get('nomor');

        if (!empty($key)) {
            $user = $this->M_Users->getUserByKey($key);
            if ($user != 0) {
                $this->response([
                    'status' => true,
                    'id' => $user['id'],
                    'nama' => $user['nama'],
                    'phone' => $user['notlp'],
                    'email' => $user['email']
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'User belum terdaftar'
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            if ($users) {
                $this->response($users);
            }
        }
    }

    public function index_post()
    {
        $nama = $this->post('nama');
        $phone = $this->post('phone');
        $email = $this->post('email');
        $role = 3;
        $date_created = time();
        $is_aktif = 1;

        $data = [
            'nama' => $nama,
            'email' => $email,
            'notlp' => $phone,
            'role_id' => $role,
            'is_aktif' => $is_aktif,
            'date_created' => $date_created
        ];

        $count = $this->M_Users->countByKey($phone);

        if ($count < 1) {
            $insert = $this->M_Users->addNewUser($data);
            if ($insert > 0) {
                $this->response([
                    'status' => true,
                    'message' => 'Yeay kamu berhasil mendaftar'
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Gagal mendaftar'
                ], REST_Controller::HTTP_TOO_MANY_REQUESTS);
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'Nomor HP telah terdaftar'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function index_put()
    {
        $nama = $this->put('nama');
        $phone = $this->put('phone');
        $email = $this->put('email');

        $data = [
            'nama' => $nama,
            'email' => $email
        ];

        $update = $this->M_Users->updateUser($data, $phone);

        if ($update > 0) {
            $this->response([
                'status' => true,
                'message' => 'Yeay berhasil merubah profil'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Gagal merubah profil'
            ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
