<?php
defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Jenis extends REST_Controller
{

	public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Jenis','jenis');
    }

    public function index_get()
    {
    	$key = $this->get('idkategori');

    	if(!empty($key)){
    		$this->response($this->jenis->getByKey($key));
    	}else{
    		$this->response($this->jenis->get());
    	}

    }
}