<?php
defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Transaksi extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_Transaksi', 'transaksi');
        $this->load->model('M_Pendapatan', 'pendapatan');
        $this->load->model('M_Produk', 'barang');
        $this->load->model('M_TransaksiDetail', 'detail');
    }

    public function index_get()
    {
    }

    public function index_post()
    {
        $act = $this->post('act');
        $kodetransaksi = 'TM-' . $this->post('kodetrans');
        $iduser = $this->post('iduser');
        $jadwalkirim = $this->post('jadwalkirim');
        $alamat = $this->post('alamat');
        $estimasi = (int) $this->post('estimasi');
        $ongkir = (int) $this->post('ongkir');
        $diskon = (int) $this->post('diskon');
        $total = (int) $this->post('total');
        $status = 1;
        $latitude = $this->post('latitude');
        $longitude = $this->post('longitude');
        $idbarang = $this->post('idbarang');
        $jumlah = $this->post('jumlah');
        $idstatus = $this->post('idstatus');
        $act = $this->post('act');


        $a_trans = [
            'kodetransaksi' => $kodetransaksi,
            'iduser' => $iduser,
            'jadwalpengiriman' => $jadwalkirim,
            'alamat' => $alamat,
            'totalestimasi' => $estimasi,
            'ongkir' => $ongkir,
            'diskon' => $diskon,
            'total' => $total,
            'idstatus' => $status,
            'latitude' => $latitude,
            'longitude' => $longitude,
        ];

        $a_detail = [
            'kodetransaksi' => $kodetransaksi,
            'idbarang' => $idbarang,
            'jumlah' => $jumlah
        ];

        $a_pendapatan = [
            'kodetransaksi' => $kodetransaksi,
            'tanggal' => date('Y-m-d H:i:s'),
            'pemasukan' => $total
        ];

        $this->transaksi->beginTrans();

        switch ($act) {
            case 'a_trans':
                $this->transaksi->insert($a_trans);
                $this->pendapatan->insert($a_pendapatan);
                break;
            case 'a_detail':
                $this->detail->insert($a_detail);
                $a_update = [
                    'stok' => getBarang()[$a_detail['idbarang']]['stok'] + $a_detail['jumlah']
                ];
                $this->barang->update($a_update, $a_detail['idbarang']);
                break;
        }

        $ok = $this->transaksi->statusTrans();
        $this->transaksi->commitTrans($ok);

        if ($ok) {
            $this->response([
                'status' => true,
                'message' => 'Data inserted!'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data not found!'
            ], REST_Controller::HTTP_CONFLICT);
        }
    }
}
