<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Promo extends REST_Controller
{


    public function __construct()
    {
        parent::__construct();
    }

    public function index_get()
    {
        $kode = $this->get('kode');
        $iduser = $this->get('iduser');
        $act = $this->get('act');
        $get = $this->db->get_where('promo', ['kodepromo' => $kode]);

        switch ($act) {
            case 'verify':
                if ($get->num_rows() > 0) {
                    //voucher valid, cek expirednya
                    $data = $get->row_array();
                    $expired = strtotime($data['validuntil']);
                    //time valid, cek voucher sudah digunakan atau belum
                    if (time() < $expired) {
                        $getKlaim = $this->db->get_where('klaimvoucher', ['idvoucher' => $data['idvoucher'], 'iduser' => $iduser]);
                        if ($getKlaim->num_rows() < 1) {
                            //insert data
                            $a_klaim = [
                                'idvoucher' => $data['idvoucher'],
                                'iduser' => $iduser,
                                'idstatusvoucher' => 1,
                            ];
                            $ok = $this->db->insert('klaimvoucher', $a_klaim);
                            if ($ok) {
                                $this->response([
                                    'status' => true,
                                    'message' => 'Berhasil menggunakan voucher'
                                ], REST_Controller::HTTP_OK);
                            } else {
                                $this->response([
                                    'status' => false,
                                    'message' => 'Gagal menggunakan voucher'
                                ], REST_Controller::HTTP_OK);
                            }
                        } else {
                            $a_data = $getKlaim->row_array();

                            if ($a_data['idstatusvoucher'] == 1) {
                                $this->response([
                                    'status' => true,
                                    'message' => 'Voucher sudah diterapkan'
                                ], REST_Controller::HTTP_OK);
                            } else if ($a_data['idstatusvoucher'] == 2) {
                                $this->response([
                                    'status' => true,
                                    'message' => 'Voucher sudah terpakai'
                                ], REST_Controller::HTTP_OK);
                            }
                        }
                    } else {
                        $this->response([
                            'status' => true,
                            'message' => 'Voucher sudah kadaluwarsa'
                        ], REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response([
                        'status' => true,
                        'message' => 'Voucher tidak valid!'
                    ], REST_Controller::HTTP_OK);
                }
                break;
            case 'get':
                $data_klaim = $this->db->get_where('klaimovoucher', ['iduser' => $iduser, 'idstatus' => 1]);
                if ($data_klaim->num_rows() > 0) {
                    $a_data = $data_klaim->row_array();
                    $this->response($a_data);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'Data Not Found'
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
                break;
        }
    }
}
