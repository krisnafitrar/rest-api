<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Swalayan extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Usaha','swalayan');
    }

    public function index_get()
    {
        $act = $this->get('act');
        $keyword = $this->get('keyword');

        switch ($act) {
            case 'get':
                $a_swalayan = $this->swalayan->getMart()->result_array();
                if ($a_swalayan) {
                    $this->response($a_swalayan);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'Data not found!'
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
                break;
            case 'search':
                $a_swalayan = $this->swalayan->searchSwalayan($keyword)->result_array();
                if ($a_swalayan) {
                    $this->response($a_swalayan);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'Data not found!'
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
                break;
        }
    }
}