<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Produk extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Produk');
    }

    public function index_get()
    {
        $key = $this->get('idkategori');
        $iduser = $this->get('iduser');
        $idusaha = $this->get('idusaha');
        $act = $this->get('act');
        $keyword = $this->get('keyword');
        $typesort = $this->get('typesort');
        $nama = $this->get('nama');

        switch ($act) {
            case 'get':
                $getProduk = $this->M_Produk->getProdukByKey($key, $idusaha)->result_array();
                if ($getProduk) {
                    $this->response($getProduk);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'Data not found!'
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
                break;
            case 'get-relevance':
                $getTrans = $this->M_Produk->getProdukByTrans($key, $iduser)->result_array();
                if ($getTrans) {
                    $this->response($getTrans);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'Data not found!'
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
                break;
            case 'search':
                $getProduk = $this->M_Produk->searchProdukByKeyword($keyword, $key, $idusaha)->result_array();
                if ($getProduk) {
                    $this->response($getProduk);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'Data not found!'
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
                break;
            case 'sort':
                $getProduk = $this->M_Produk->sortProdukByKey($key, $typesort, $idusaha, $keyword)->result_array();
                if ($getProduk) {
                    $this->response($getProduk);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'Data not found!'
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
                break;
            case '':
                $this->response([
                    'status' => false,
                    'message' => 'Provide an act!'
                ], REST_Controller::HTTP_NOT_FOUND);
                break;
            case 'getByName':
                $val = $this->M_Produk->getProdukByName($nama);
                if ($val) {
                    $this->response([
                        'status' => true,
                        'idbarang' => $val['idbarang'],
                        'data' => $val
                    ], REST_Controller::HTTP_NOT_FOUND);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'Data not found!'
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
                break;
        }
    }
}
