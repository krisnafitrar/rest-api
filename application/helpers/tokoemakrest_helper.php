<?php

function getBarang()
{
    $ci = get_instance();
    $ci->load->model('M_Produk', 'barang');

    $barang = $ci->barang->get()->result_array();
    $a_data = array();

    foreach ($barang as $val) {
        $a_data[$val['idbarang']] = $val;
    }

    return $a_data;
}
