<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->db->db_debug = false;
    }

    public function listfields()
    {

        $query = $this->db->query('SELECT * FROM ' . $this->getTable() . ' limit 1');

        $fields = array();
        foreach ($query->list_fields() as $field) {
            $fields[] = $field;
        }

        return $fields;
    }

    public function getTable($table = null)
    {
        if (!empty($table))
            $this->table = $table;

        if (!empty($this->schema) and empty($table)) {
            return $this->schema . '.' . $this->table;
        }

        return $this->table;
    }

    public function join($table = [], $index = [], $type = [], $condition = [], $orderby = [])
    {
        $args = "";
        $cond = "";
        $sort = "";
        $shuffle = str_shuffle($this->getTable());
        $chr = substr($shuffle, strlen($shuffle) - 2, strlen($shuffle));
        if (!empty($condition)) {
            foreach ($condition as $c => $value) {
                $cond .=  $c . "='" . $value . "' AND ";
            }
        }
        for ($x = 0; $x < count($table); $x++) {
            for ($y = $x; $y <= $x; $y++) {
                $key = $this->getPK($table[$x]);
                $x_shuffle = str_shuffle($table[$x]);
                $cut_str = substr($x_shuffle, strlen($x_shuffle) - 2, strlen($x_shuffle));
                $jtype = !empty($type[$y]) ? " " . strtoupper($type[$y]) . " " : " JOIN ";
                $args .= $jtype . $table[$x] . ' ' . $cut_str . " ON " . $chr . "." . "$index[$y]" . "=" . $cut_str . "." . $key;
            }
        }

        if (!empty($orderby)) {
            foreach ($orderby as $o => $val) {
                $sort .= " ORDER BY " . $o . " " . $val;
            }
        }

        $z = !empty($cond) ? " WHERE " . substr($cond, 0, strlen($cond) - 5) : "";
        $query = "SELECT * FROM " . $this->getTable() . ' ' . $chr . $args . $z . $sort;

        return $this->db->query($query);
    }

    public function getPK($table = null)
    {
        $fields = $this->db->list_fields($table != null ? $table : $this->table);
        foreach ($fields as $field) {
            return $field;
        }
    }

    public function getKey()
    {
        $a_key = explode(',', $this->key);

        foreach ($a_key as $k => $v) {
            $a_key[$k] = trim($v);
        }

        if (count($a_key) == 1)
            return $this->key;
        else
            return $a_key;
    }

    // get key from row/data
    public function getPagerKey($row, $separator = '/')
    {
        $keycol = $this->getKey();

        if (!is_array($keycol))
            $keycol = array($keycol);

        foreach ($keycol as $key) {
            if (!empty($row[$key]))
                $a_key[] = $row[$key];
        }

        return implode($separator, $a_key);
    }

    public function insert($record, $returning = false)
    {
        $fields = $this->listfields();

        $rec = array();
        foreach ($fields as $field) {
            if (!empty($record[$field])) {
                $rec[$field] = $record[$field];
            }
        }

        if ($returning) {
            $err = $this->db->insert($this->getTable(), $rec);
            return array($err, $this->db->insert_id());
        } else {
            return $this->db->insert($this->getTable(), $rec);
        }
    }

    public function update($record, $key, $returning = false)
    {
        $keys = explode('/', $key);
        $a_key = $this->getKey();
        $fields = $this->listfields();

        $where = array();

        if (!is_array($a_key))
            $a_key = array($a_key);

        foreach ($a_key as $k => $v) {
            $where[$v] = $keys[$k];
        }

        $rec = array();
        foreach ($fields as $field) {
            if (isset($record[$field]) and $record[$field] == '0')
                $rec[$field] = $record[$field];

            if (!empty($record[$field]))
                $rec[$field] = $record[$field];
        }

        $this->db->where($where);
        $data = $this->db->get($this->getTable())->row_array();

        // cek perbedaan value di table
        foreach ($rec as $x => $row) {
            if ($row == $data[$x])
                unset($rec[$x]);
        }

        if (!empty($rec)) {
            $this->db->set($rec);
            $this->db->where($where);
            if ($returning)
                return array($this->db->update($this->getTable()), $this->getPagerKey($data));
            else
                return $this->db->update($this->getTable());
        } else {
            if ($returning)
                return array(true, $this->getPagerKey($data));
            else
                return true;
        }
    }

    public function delete($key)
    {
        $keys = explode('/', $key);
        $a_key = $this->getKey();

        $where = array();

        if (!is_array($a_key))
            $a_key = array($a_key);

        foreach ($a_key as $k => $v) {
            $where[$v] = $keys[$k];
        }

        $this->db->where($where);
        return $this->db->delete($this->getTable());
    }

    public function get($key = null)
    {
        if (!empty($key)) {
            $keys = explode('/', $key);
            $a_key = $this->getKey();

            $where = array();

            if (!is_array($a_key))
                $a_key = array($a_key);

            foreach ($a_key as $k => $v) {
                $where[$v] = $keys[$k];
            }

            $this->db->where($where);
        }
        return $this->db->get($this->getTable());
    }

    public function getBy($where)
    {
        $this->db->where($where);
        return $this->db->get($this->getTable());
    }

    public function getCondition($where = array())
    {
        $cond = array();
        foreach ($where as $k => $v) {
            $cond[$k] = $v;
        }

        return $cond;
    }

    public function getListCombo($empty = false, $sort = 'asc')
    {
        $this->db->order_by($this->key, $sort);

        $data = $this->get()->result_array();

        $a_data = array();
        if ($empty)
            $a_data[''] = '-- Pilih Semua--';

        foreach ($data as $val)
            $a_data[$val[$this->key]] = $val[$this->value];

        return $a_data;
    }

    public function beginTrans()
    {
        $this->db->trans_start();
    }

    public function statusTrans()
    {
        return $this->db->trans_status();
    }

    public function commitTrans($ok)
    {
        if ($ok) {
            $this->db->trans_commit();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function getPagerData($start = 0, $limit = 10)
    {
        return $this->db->get($this->table, $limit, $start)->result_array();
    }

    public function getAllRows()
    {
        return $this->db->from($this->table)->count_all_results();
    }

    public function getAI()
    {
        $db = $this->db->database;
        $query = "SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA ='$db' AND TABLE_NAME='$this->table'";
        return (int) $this->db->query($query)->result_array()[0]['AUTO_INCREMENT'];
    }
}
